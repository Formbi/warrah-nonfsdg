(define-module (warrah-nonfsdg packages unetbootin)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages mtools))

(define-public unetbootin
  (package
    (name "unetbootin")
    (version "675")
    (source
     (origin
      (method url-fetch/tarbomb)
       (uri (string-append "https://sourceforge.net/projects/unetbootin/files/UNetbootin/"
                           version "/unetbootin-source-" version ".tar.gz"))
       (sha256
        (base32
         "0vanyvc43akvw4anal0d4cbg1s33ifqb93v6yn6i4a6z8bf13q3c"))))
    (build-system gnu-build-system)
    (inputs
     `(("qt" ,qt-4)
       ("mtools" ,mtools)
       ("syslinux" ,syslinux)
       ("p7zip" ,p7zip)))
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
       
         (replace 'configure
           (lambda _
             (let* ((out (assoc-ref %outputs "out"))
                    (prefix (string-append "DESTDIR=" out "/bin"))
                    (lupdate (string-append (assoc-ref %build-inputs "qt") "/bin/lupdate"))
                    (lrelease (string-append (assoc-ref %build-inputs "qt") "/bin/lrelease"))
                    (qmake (string-append (assoc-ref %build-inputs "qt") "/bin/qmake")))
               (invoke "chmod" "-R" "+w" ".")
               (invoke lupdate "unetbootin.pro")
               (invoke lrelease "unetbootin.pro")
               (invoke qmake prefix)
               #t)))
               
         (add-after 'install 'wrap-program
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (p7zip (assoc-ref inputs "p7zip"))
                    (mtools (assoc-ref inputs "mtools"))
                    (syslinux (assoc-ref inputs "syslinux"))
                    (path (string-append syslinux "/bin:"
                                         syslinux "/sbin:"
                                         mtools "/bin:"
                                         p7zip "/bin")))
               (wrap-program (string-append bin "/unetbootin")
                 `("PATH" prefix (,path))
                 `("QT_X11_NO_MITSHM" = (,"1")))
               #t)))
           
         )))
    (synopsis "Live USB creator")
    (description "UNetbootin allows you to create bootable Live USB drives for GNU/Linux distributions without burning a CD.")
    (home-page "https://unetbootin.github.io/")
    (license gpl2+)))

unetbootin
